Java Android SafetyNet Sample
===================================

This is based on the

For more details, see the guide at https://developer.android.com/training/safetynet/index.html.

Pre-requisites
--------------

- Android SDK 25
- Latest Android Build Tools
- Latest Google Play Services
- Android Support Repository


Getting Started
---------------

You need to set up an API key for the SafetyNet attestation API and reference it in this project.
Follow the steps in the [SafetyNet Attestation API][add-api-key] guide to set up an API key in the
Google Developers console. Then, override the configuration in the `gradle.properties` file to set
the key. This value is used for the call to
<a href="https://developers.google.com/android/reference/com/google/android/gms/safetynet/SafetyNetClient.html#attest(byte[], java.lang.String)">`SafetyNetClient# attest()`</a>.



[add-api-key]: https://developer.android.com/training/safetynet/attestation.html#add-api-key



The results of the online verification:


The content of the attestation statement is:
Nonce: [-100, 26, 104, -113, 119, -61, 32, -118, 68, -83, -127, -90, -98, -65, 119, -72, -120, 82, -7, -2, -57, -57, 23, 110, 83, 97, 102, 101, 116, 121, 32, 78, 101, 116, 32, 83, 97, 109, 112, 108, 101, 58, 32, 49, 53, 56, 55, 52, 57, 53, 55, 49, 57, 52, 56, 56]
Timestamp: 1587495721219 ms
APK package name: com.example.android.safetynetsample
APK digest SHA256: [27, 35, -56, 98, 125, -90, 34, 61, 4, -123, 31, -6, 69, -40, -101, 13, 14, 101, 109, -92, 58, -92, -14, -35, 62, -9, 28, -117, -63, 18, 40, 110]
CTS profile match: true
Basic integrity match: true

** This sample only shows how to verify the authenticity of an attestation response. Next, you must check that the server response matches the request by comparing the nonce, package name, timestamp and digest.
Disconnected from the target VM, address: '127.0.0.1:65032', transport: 'socket'

Process finished with exit code 0
